<?php if($thumbs): ?>
  <div id="galleriffic-controls"></div>
  <div id="galleriffic-loading"></div>
  <div id="galleriffic-slideshow"></div>
  <div id="galleriffic-caption"></div>
  <div id="galleriffic-thumbs"> 
    <ul class="thumbs noscript">
      <?php foreach ($thumbs as $delta => $thumb): ?>
      <li>
        <a class="thumb" name="galleriffic-fid-<?php print $thumb['fid']; ?>" href="<?php print $thumb['url']['medium']; ?>" title="<?php print $thumb['title']; ?>">
          <img src="<?php print $thumb['url']['thumb']; ?>" alt="<?php print $thumb['title']; ?>" />
        </a>
        <div class="caption"></div>
      </li>
      <?php endforeach; ?>
    </ul>
  </div>
<?php endif; ?>